package edu.url.salle.controller;

import java.util.List;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import edu.url.salle.form.TipoCarneListaForm;
import edu.url.salle.model.TipoCarne;
import edu.url.salle.service.TipoCarneService;

@Controller
@RequestMapping(value = {"/tipocarne"})
public class TipoCarneController {

	@Autowired
	private TipoCarneService tipoCarneService;
	
	
	private static final Logger logger = (Logger) LoggerFactory.logger(TipoCarneController.class);
	
	@RequestMapping(method = RequestMethod.GET)
	public final ModelAndView listarTiposCarne() {
		
		logger.info("Metodo de listar tipos de carne");
		
		List<TipoCarne> listaTipoCarne = tipoCarneService.listarTiposCarne();
		
		TipoCarneListaForm tipoCarneListaForm = new TipoCarneListaForm();
		
		tipoCarneListaForm.setTiposCarne(listaTipoCarne);
		
		
		if(!listaTipoCarne.isEmpty()) {
			logger.info(listaTipoCarne.get(0).getNombre());
		}
		
		return new ModelAndView("tipoCarneLista", "tipoCarneListaForm", tipoCarneListaForm);
	}
	
}
